import React, { Component } from 'react'
import {
  Segment,
  Table,
  Input,
  Button,
  Form,
  Step
} from 'semantic-ui-react'
import axios from 'axios'

import CreateTournament from '../../components/CreateTournament/CreateTournament'
import AddUsers from '../../components/AddUsers/AddUsers'
import Settings from '../../components/Settings/Settings'
import Finalize from '../../components/Finalize/Finalize'

export class Singles extends Component {
  state = {
    singlesUrl: '',
    api_key: 'xJs0FWU1ZbOo84PHnZyKwE3CG8QVf1aOxJEuBjQO',
    activeStep: 'create',
  }

  getNextStep = (nextStep) => {
    this.setState({
      activeStep: nextStep
    })
  }
  
  renderActiveStep() {
    switch(this.state.activeStep) {
      case 'create':
        return <CreateTournament 
        bracket='singles' 
        getNextStep={this.getNextStep}
        />
        case 'users':
        return <AddUsers 
        bracket='singles' 
        getNextStep={this.getNextStep}
        />
        case 'settings':
        return <Settings 
        bracket='singles' 
        getNextStep={this.getNextStep}
        />
        case 'finalize': 
        return <Finalize 
        bracket='singles' 
        getNextStep={this.getNextStep}
        />
      default:
        return null
    }
  }

  componentDidMount() {
    if(this.state.CurrentStep !== 'create') {
      axios.get('http://localhost:8080/bracket/singles/currentStep')
      .then(res => {
        this.setState({activeStep: res.data.CurrentStep})
      })
    }
  }

  render() {
    const activeStep = this.state.activeStep

    return (
      <Segment basic="basic" ID='SinglesSignup'>
        <Segment>
          <Step.Group ordered attached='top'>
            <Step
              className='create' 
              active={activeStep === 'create'}
              title='Creat Bracket' />
            <Step 
              className='users' 
              active={activeStep === 'users'}
              title='Add Users'
              description='Add users to your bracket' /> 
            <Step 
              className='settings' 
              active={activeStep === 'settings'}
              title='Settings'
              description='Fill out the settings for your bracket' /> 
            <Step
              className='finalize' 
              active={activeStep === 'finalize'}
              title='Finalize'
              description='Finalize the bracket' /> 
          </Step.Group>

          <Segment attached>
            {this.renderActiveStep()}
          </Segment>
        </Segment>
      </Segment>
    )
  }
}

export default Singles