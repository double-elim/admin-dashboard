import React, { Component } from 'react';
import ChartistGraph from 'react-chartist';
import { Grid, Row, Col } from 'react-bootstrap';

import { PulseLoader } from 'react-spinners';
import axios from 'axios';

import {Card} from 'components/Card/Card.jsx';
import {StatsCard} from 'components/StatsCard/StatsCard.jsx';
import {Tasks} from 'components/Tasks/Tasks.jsx';
import {
    dataPie,
    legendPie,
    dataSales,
    optionsSales,
    responsiveSales,
    legendSales,
    dataBar,
    optionsBar,
    responsiveBar,
    legendBar
} from 'variables/Variables.jsx';

class Dashboard extends Component {
    state = {
        singlesId: '',
        doublesId: '',
        loadingSinglesInfo: true,
        loadingDoublesInfo: true
    }

    renderBracketStatus(bracketLoading, bracketId, bracketUrl) {
        return (bracketLoading === true) ?
             <PulseLoader />
            : (bracketId !== '') ?
                <iframe
                    src={bracketUrl}
                    width="100%"
                    height="500"
                    frameBorder="0"
                    scrolling="no"
                />
            :
                <div>
                    Bracket has not started
                </div>
            
        
    }
    
    getSinglesInfo() {
        axios.get(`http://localhost:8080/bracket/singles/info`)
          .then(res => {
              this.setState({
                  singlesId: res.data.ID,
                  loadingSinglesInfo: false
              })
          })
          .catch(err => {
              console.log(err)
              this.setState({
                  loadingSinglesInfo: false
              })
          })
    }

    componentDidMount(){
        this.getSinglesInfo('singles', 'singlesId', this.state.loadingSinglesInfo);
    }

    render() {
        const singlesId = this.state.singlesId
        const singlesUrl = `https://revivalsam.challonge.com/${singlesId}/module?show_standings=1&tab=standings`
        const doublesId = this.state.doublesId
        const doublesUrl = `https://revivalsam.challonge.com/${doublesId}/module?show_standings=1&tab=standings`

        return (
            <div className="content">
                <Grid fluid>
                    <Row>
                        <Col lg={3} sm={6}>
                            <StatsCard
                                bigIcon={<i className="pe-7s-user text-info"></i>}
                                statsText="Entrants Remaining"
                                statsValue="10"
                                statsIcon={<i className="fa fa-refresh"></i>}
                                statsIconText="Updated now"
                                bracket="Singles"
                            />
                        </Col>
                        <Col lg={3} sm={6}>
                            <StatsCard
                                bigIcon={<i className="pe-7s-user text-danger"></i>}
                                statsText="Entrants Eliminated"
                                statsValue="6"
                                statsIcon={<i className="fa fa-refresh"></i>}
                                statsIconText="Updated now"
                                bracket="Singles"
                            />
                        </Col>
                        <Col lg={3} sm={6}>
                            <StatsCard
                                bigIcon={<i className="pe-7s-users text-info"></i>}
                                statsText="Teams Remaining"
                                statsValue="23"
                                statsIcon={<i className="fa fa-refresh"></i>}
                                statsIconText="Updated now"
                                bracket="Doubles"
                            />
                        </Col>
                        <Col lg={3} sm={6}>
                            <StatsCard
                                bigIcon={<i className="fa pe-7s-users text-danger"></i>}
                                statsText="Teams Eliminated"
                                statsValue="5"
                                statsIcon={<i className="fa fa-refresh"></i>}
                                statsIconText="Updated now"
                                bracket="Doubles"
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6}>
                            <Card
                                statsIcon="fa pe-7s-info"
                                id="twitchChat"
                                title="Twitch Preview"
                                stats="6 active viewers"
                                content={
                                    <iframe
                                        frameBorder="0"
                                        scrolling="no"
                                        height="360px"
                                        width="100%"
                                        src="http://player.twitch.tv/?ut_okayp"
                                    />
                                }
                            />
                        </Col>
                        <Col md={6}>
                            <Card
                                statsIcon="fa pe-7s-info"
                                id="twitchChat"
                                title="Twitch Chat"
                                stats="6 active viewers"
                                content={
                                    <iframe
                                        frameBorder="0"
                                        scrolling="no"
                                        height="360px"
                                        width="100%"
                                        src="https://www.twitch.tv/embed/ut_okayp/chat"
                                    />
                                }
                            />
                        </Col>
                    </Row>

                    <Row>
                        <Col md={6}>
                            <Card
                                id="chartActivity"
                                title="Standings"
                                category="Singles"
                                content={
                                    this.state.loadingSinglesInfo
                                        ? <PulseLoader />
                                        : singlesId !== ''
                                            ?
                                            <iframe
                                                src={singlesUrl}
                                                width="100%"
                                                height="500"
                                                frameBorder="0"
                                                scrolling="no"
                                            />
                                            :
                                            <div>
                                                Bracket has not started.
                                            </div>
                                }
                                
                            />
                        </Col>

                        <Col md={6}>
                            <Card
                                id="chartActivity"
                                title="Standings"
                                category="Doubles"
                                content={
                                    this.state.loadingDoublesInfo
                                        ? <PulseLoader />
                                        : doublesId !== ''
                                            ?
                                            <iframe
                                                src={doublesUrl}
                                                width="100%"
                                                height="500"
                                                frameBorder="0"
                                                scrolling="no"
                                            />
                                            :
                                            <div>
                                                Bracket has not started.
                                            </div>
                                }
                            />
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}

export default Dashboard;
